package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class DashboardPage extends ProjectMethods {
	Actions builder=new Actions(driver);
	public DashboardPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.XPATH,using="//div[5]//button[1]")
	WebElement eleVendors;

	@FindBy(how=How.LINK_TEXT,using="Search for Vendor")
	WebElement eleSearchForVendor;

	public DashboardPage moveToVendors() {

		builder.moveToElement(eleVendors).perform();
		return this;
	}
	public VendorSearchPage searchForVendors()
	{
		clickWithNoSnap(eleSearchForVendor);
		return new VendorSearchPage();
	}
	
}
