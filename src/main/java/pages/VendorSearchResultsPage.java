package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class VendorSearchResultsPage extends ProjectMethods{

	public VendorSearchResultsPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void getText() {
		WebElement table = driver.findElementByClassName("table");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		List<WebElement> eachCol = rows.get(1).findElements(By.tagName("td"));
		String VendorName = eachCol.get(0).getText();
		System.out.println(VendorName);	
	}
}
