package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class VendorSearchPage extends ProjectMethods {

	public VendorSearchPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using="vendorTaxID")
	WebElement vendorIDField;
	
	@FindBy(how=How.ID,using="buttonSearch")
	WebElement eleSearchBtn;
	
	public VendorSearchPage enterVendorID(String data) {
		clearAndType(vendorIDField, data);
		return this;
	}
	public VendorSearchResultsPage clickSearchBtn() {
		clickWithNoSnap(eleSearchBtn);
		return new VendorSearchResultsPage();
	}
}
