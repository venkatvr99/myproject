package pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class LoginPage extends ProjectMethods{
	public LoginPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.CLASS_NAME,using="form-control")
	WebElement eleEmail;

	@FindBy(how=How.XPATH,using="//input[@id='password']")
	WebElement elePassword;

	@FindBy(how=How.ID,using="buttonLogin")
	WebElement eleLoginBtn;

	public LoginPage enterEmail(String data) {
		clearAndType(eleEmail, data);
		return this;
	}
	public LoginPage enterPassword(String data) {
		clearAndType(elePassword, data);
		return this;
	}
	public DashboardPage clickLogin()
	{
		clickWithNoSnap(eleLoginBtn);
		return new DashboardPage();
	}
}

