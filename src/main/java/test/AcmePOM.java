package test;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;

import pages.LoginPage;

public class AcmePOM extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName="TC01_GetVendorName";
		testDescription="To get the vendor name";
		author="Venkat";
		testNodes="Leads";
		category="functional";
		dataSheetName="VendorName";
	}
	@Test(dataProvider="fetchData")
	public void getVendorName(String email,String password,String vendorID) {
		new LoginPage().enterEmail(email)
		.enterPassword(password)
		.clickLogin()
		.moveToVendors()
		.searchForVendors()
		.enterVendorID(vendorID)
		.clickSearchBtn()
		.getText();
	}
}
