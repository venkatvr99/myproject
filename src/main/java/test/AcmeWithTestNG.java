package test;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class AcmeWithTestNG {
	@Test
	public void Acme() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "./drivers//chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementByClassName("form-control").sendKeys("venkycoolbuddy009@gmail.com");
		driver.findElementByXPath("//input[@id='password']").sendKeys("9788139973");
		driver.findElementById("buttonLogin").click();
		Thread.sleep(3000);
		Actions builder=new Actions(driver);
		WebElement eleVendors = driver.findElementByXPath("//div[5]//button[1]");
		//WebElement searchForVendor = driver.findElementByLinkText("Search for Vendor");
		builder.moveToElement(eleVendors).perform();
		driver.findElementByLinkText("Search for Vendor").click();
		driver.findElementById("vendorTaxID").sendKeys("RO212121");
		driver.findElementById("buttonSearch").click();
		WebElement table = driver.findElementByClassName("table");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		List<WebElement> eachCol = rows.get(1).findElements(By.tagName("td"));
		String VendorName = eachCol.get(0).getText();
		System.out.println(VendorName);	
		driver.close();
	}
}
